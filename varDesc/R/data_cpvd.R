
#'
#'Default look up table of specific functions being called for each type of pairwise tests 
#'col(row) names correspond to the types of variables that are supported for the analysis 
#'
#'
#' @format 3 X 3 matrix :
#' \describe{
#'   \item{factor}
#'   \item{numeric}
#'   \item{Surv}
#' }
#' 
#' 
"lookup.default"

