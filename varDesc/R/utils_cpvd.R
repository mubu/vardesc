#util functions for function cupavade

voidf<-function(aaa,bbb,var.names=NA){
  
  re<-list()
  re$p.value<-NA
  re$estimate<-NA
  re$test<-"UNDEFINED" 
  re$stats<-NA
  
  return(re)
}

fa.fa<-function(fac1,fac2,var.names=NA){
  #factor_factor
  valid=!(is.na(fac1)|is.na(fac2))
  
  fac1=fac1[valid]
  fac2=fac2[valid]
  
  
  tab=table(fac1,fac2,dnn=var.names)
  
  tried<-try(ft<-fisher.test(tab),silent = T)
  if(inherits(tried, "try-error")){
    tried<-try(ft<-fisher.test(tab,simulate.p.value = T,B=1e7),silent = T)
    if(inherits(tried, "try-error")){
      message("something wrong for fisher !...")
      ft<-list(p.value=NA)
    }
  }
  
  if(is.null(ft$estimate)) ft$estimate <-NA
  
  re<-list()
  re$p.value<-ft$p.value
  re$estimate<-ft$estimate
  re$test<-"fisher.test" 
  re$stats<-ft
  
  return(re)
}

fa.nu<-function(fac,num,var.names=NA){
  #factor_numeric # anova
  
  valid=!(is.na(fac)|is.na(num))
  
  fac=fac[valid]
  num=num[valid]
  
  anv= summary(aov(num~fac))
  pval=anv[[1]][["Pr(>F)"]][[1]]
  re<-list()
  re$p.value<-pval
  re$estimate<-NA
  re$test<-"aov" #re$estimate<-NA
  re$stats<-anv
  
  return(re)
} 

fa.Su<-function(fac,sur,var.names=NA){
  
  valid=!(is.na(fac)|is.na(sur))
  fac=fac[valid]
  sur=sur[valid]
  
  #factor_Surv #log-rank test 
  sdiff <- survival::survdiff(sur~fac)
  pval <- stats::pchisq(sdiff$chisq, length(sdiff$n) - 1, lower.tail = FALSE)
  re<-list()
  re$p.value<-pval
  re$estimate<-NA
  re$test<-"log-rank" #re$estimate<-NA
  re$stats<-sdiff
  
  return(re)
}

nu.nu<-function(num1,num2,var.names=NA){
  valid=!(is.na(num1)|is.na(num2))
  
  num1=num1[valid]
  num2=num2[valid]
  
  #numeric_numeric #correlation test
  cc=cor.test(num1,num2,method = "pearson")
  re<-list()
  re$p.value<-cc$p.value
  re$estimate<-cc$estimate
  re$test<-"pearson" 
  re$stats<-cc
  
  return(re)
}

nu.Su<-function(num,sur,var.names){
  #numeric_Surv #dichotomize + logrank-test 
  dich<-cut(num,breaks = quantile(num,probs = c(0,0.33,0.67,1)),
            labels = c("low","mid","high"))
  return(fa.Su(dich,sur))
}

Su.Su<-function(sur1,sur2,var.names=NA){
  
  valid=!(is.na(sur1)|is.na(sur2))
  
  sur1=sur1[valid]
  sur2=sur2[valid]
  
  #Surv_Surv my implemantation based on comp pairs
  #comparable pairs of each variable
  au1<-aum(sur1)
  au2<-aum(sur2)
  
  m1=paste(au1[,1],au1[,2],sep = "-")
  m2=paste(au2[,1],au2[,2],sep = "-")
  uni=union(m1,m2)     #union of comp.pairs
  int=intersect(m1,m2) #commom comp pairs
  
  
  stat<-sapply(list(setdiff(m1,int),int,setdiff(m2,int)),length)
  names(stat)<-c(paste0(var.names[1],"_"),paste(var.names[1],var.names[2],sep = "n"),paste0(var.names[2],"_"))
  
  
  re<-list()
  re$p.value<-NA
  re$estimate<-length(int)/length(uni)
  re$test<-"concordance"
  re$stats<-stat
  
  return(re)
  
}

# finds the comparable pairs
aum<-function(stest,rm.tied=T){
  N=dim(stest)[1]
  
  smap<-matrix(0,N*(N-1)/2)
  emap<-matrix(0,N*(N-1)/2)
  emap0<-matrix(0,N*(N-1)/2)
  pairs<-matrix(0,N*(N-1)/2,2)
  
  j=length(smap)-1
  ps<-1
  pe<-N-1
  for(i in c(1:pe)){
    pairs[c(ps:pe),1]<-c((i+1):N)
    pairs[c(ps:pe),2]<-i
    smap[c(ps:pe)]<-stest[c((i+1):N),1]-stest[i,1]
    emap[c(ps:pe)]<-stest[c((i+1):N),2]-stest[i,2]
    emap0[c(ps:pe)]<-stest[c((i+1):N),2]+stest[i,2]
    
    
    df=pe-ps-1
    ps=pe+1
    pe=ps+df
  }
  
  unc.pairs<-c(which(as.logical((emap==1)*(smap>0))),which(as.logical((emap==-1)*(smap<0))),which(emap0==0))
  lup=length(unc.pairs)
  
  
  if(lup>0){
    pairs<-pairs[-unc.pairs,]
  }
  shuffle.inds<-stest[pairs[,1],1]<stest[pairs[,2],1]
  pairs[shuffle.inds,]<-pairs[shuffle.inds,c(2,1)]
  
  if(rm.tied){
    rm.inds<-stest[pairs[,1],1]==stest[pairs[,2],1]
    pairs<-pairs[!rm.inds,]
  }else{
    rm.inds<-stest[pairs[,1],1]==stest[pairs[,2],1]
    add<-pairs[rm.inds,]
    pairs<-rbind(pairs,add[,c(2,1)])
  }
  
  return(pairs)
}
