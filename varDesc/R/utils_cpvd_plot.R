
voidf_gg<-function(aaa,bbb,var.names=NA){
  gg<-ggplot2::ggplot()+ggplot2::labs(x.labs=var.names[1],ylabs=var.names[2])
  return(gg)
}

fa.fa_gg<-function(fac1,fac2,var.names=NA){
  #factor_factor
  
  tab=table(fac1,fac2,dnn=var.names)
  
  cont = as.data.frame(tab)
  #tried<-try(pval<-fisher.test(tab)$p.value,silent = T)
  
  # contigency table test+bubble plot with squares
  gg<-ggplot2::ggplot(cont, ggplot2::aes_string(x=var.names[1],y=var.names[2],size="Freq"))+
    ggplot2::geom_point(shape=22,fill="white")+
    ggplot2::geom_text(size=4,ggplot2::aes(label=Freq))+
    # scale the size of the point by area
    ggplot2::scale_size_area(max_size=40,guide="none")+
    ggplot2::theme_bw()
  
  return(gg)
}

fa.nu_gg<-function(fac,num,var.names=NA){
  #factor_numeric 
  dfbox<-data.frame(fac,num)
  colnames(dfbox)<-var.names
  
  gg<-ggplot2::ggplot(dfbox, ggplot2::aes_string(var.names[1],var.names[2]))+
    ggplot2::geom_boxplot()
  
  return(gg)
} 

fa.Su_gg<-function(fac,sur,var.names=NA){
  
  df=data.frame(fac,sur)
  # colnames(df)<-var.names
  # colnames(df)[2]<-"surv"
  # forml<-(paste0(var.names[c(2,1)],collapse = "~"))
  # print(forml)
  
  sf<-survival::survfit(sur ~ fac)
  p<-survminer::ggsurvplot(sf,conf.int = TRUE,data = df,pval = TRUE)
  gg<-p$plot
  
  return(gg)
}

nu.nu_gg<-function(num1,num2,var.names=NA){
  #numeric_numeric #correlation test
  
  df=data.frame(num1,num2)
  colnames(df)<-var.names
  gg = ggplot2::ggplot(df,ggplot2::aes_string(x=var.names[1],y=var.names[2])) +
       ggplot2::geom_point() +
       ggplot2::geom_smooth(method=lm, se=F)
  
  return(gg)
}

nu.Su_gg<-function(num,sur,var.names){
  #numeric_Surv #dichotomize + logrank-test 
  dich<-cut(num,breaks = quantile(num,probs = c(0,0.33,0.67,1)),
            labels = c("low","mid","high"))
  return(fa.Su_gg(dich,sur,var.names=var.names))
}

Su.Su_gg<-function(sur1,sur2,var.names=NA){
   sta<-Su.Su(sur1,sur2,var.names=var.names)$stats
   lbls<-c(names(sta),all)
   
   nCompPairs<-cumsum(c(0,sta))
   nPairs<-dim(sur1)[1]
   nPairs<-nPairs*(nPairs-1)/2
   return(ggplot2::ggplot())
   
}

tostr<-function(node){
  pv=paste("p.value: ",sprintf("%.2g", node$p.value))
  es=paste("estimate : ",sprintf("%.2g", node$estimate))
  ts=paste("test: ",node$test)
  
  subtit=paste(c(pv,es,ts),collapse = " ,")
  tit=paste(node$vNames,collapse = " - ")
  return(list(tit=tit,subtit=subtit))
}