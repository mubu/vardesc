# varDesc package
mu.bu.  
`r format(Sys.time(), '%d %B, %Y')`  



## Installing `varDesc-package`


```r
install.packages("path-to-file/varDesc_1.0.tar.gz", repos = NULL)
```

## varDesc package

`varDesc-package` is the replacement of `describeClinicals` function, which is written by Jan and me. Besides keeping core functionalities of the old version, this package includes some new functionalities. 

Main purpose is to get statistics (and optionally plots as well) for all pairwise comparisons in a given data frame. Simply, for a given `data.frame` consisting of different type of variables (i.e. `factor`, `numeric`, `Surv` etc.), it gives `p-values`, and `estimates` containing corresponding p values and statistical estimates for all pairs of variables. For example, function applies Fisher test and returns p-value and estimate of the test for `factor-factor` and it returns results of an ANOVA test in the case of comparing factor and numeric variables.    

### What's new

- It is fully customizable. You can customize both tests being done and plotting according to your purposes.
- It is parallelazible. 

## Customizable Pairwise Variable Descripton(cpvd)

Main function of the package is `cpvd(...)`. This is the skeleton:


```r
cpvd<-function(dta,lookup=NULL,vis=FALSE,lookup.vis=NULL,cl4par=NULL){
  #-arguments:
  #dta: data.frame
  #lookup: lookup table for the tests
  #vis: flag for plotting
  #lookup.vis: lookup table for the plots
  #cl4par: clusters for parallel computing 
  
  
  #-outputs:
  ree<-list()
  ree$p.values<-pmat # matrix of p-values 
  ree$estimates<-emat # matrix of estimates
  ree$stats<-aa # collection of objects returned by statistical tests
  ree$plots<-gg # matrix of ggplots for visualization 
  
  return(ree)
}
```

**lookup:** is a matrix storing function names that are being applied to combinations of supported variable types. For example, default look-up table(lup) for statistical tests looks like 


```
##         factor  numeric Surv   
## factor  "fa.fa" "fa.nu" "fa.Su"
## numeric "fa.nu" "nu.nu" "nu.Su"
## Surv    "fa.Su" "nu.Su" "Su.Su"
```

Here, `fa.nu(...)` is a function of the test being applied for a pair of `factor,numeric` variables. `cpvd` selects proper function from lup to apply a pair of variables. So, this provides easy customization to the package that changing function names in the lup allows user to configure the pipeline. Logic is the same for plotting as well. 

### Default settings 
Default test settings in the package are;

- `factor`-`factor`: Fisher test
- `factor`-`numeric`: ANOVA
- `factor`-`Surv`: log-rank test
- `numeric`-`numeric`: Pearson correlation
- `numeric`-`Surv`: log-rank tests on thirtiles.
- `Surv`-`Surv`: concordance(my implementation)

Toy data: 

```r
  rm(list=ls())  
  library(survival)

  set.seed(42);
  dta=data.frame(Vn1=runif(100),
                 Vf1=factor(sample(c("mu","bu"),100,replace = T)),
                 Vn2=runif(100),
                 Vf2=factor(cut(sample(100),breaks = c(0,10,60,100),labels = c("chili","orange","banana"))),
                 Vs1=sgi::kirc.clinical$survival[sample(400,100)],
                 Vs2=sgi::kirc.clinical$survival[sample(400,100)])
                 
  
  head(dta,10)
```

```
##          Vn1 Vf1       Vn2    Vf2   Vs1   Vs2
## 1  0.9148060  bu 0.8851177 orange 1106+  238 
## 2  0.9370754  mu 0.5171111 orange  645   510 
## 3  0.2861395  mu 0.8519310  chili 2172+    2 
## 4  0.8304476  mu 0.4427963 orange  141+  431 
## 5  0.6417455  bu 0.1578801 banana 1200    43 
## 6  0.5190959  bu 0.4423246 banana 1481+ 1450+
## 7  0.7365883  bu 0.9677337 orange 2799+ 1343 
## 8  0.1346666  bu 0.4845879 orange  245   600 
## 9  0.6569923  bu 0.2524584 orange  574+ 1520+
## 10 0.7050648  mu 0.2596900 orange  362+  362
```

```r
  sapply(dta,class)
```

```
##       Vn1       Vf1       Vn2       Vf2       Vs1       Vs2 
## "numeric"  "factor" "numeric"  "factor"    "Surv"    "Surv"
```

```r
  # running cpvd with default settings
  vad = varDesc::cpvd(dta = dta,vis = T)
  
  # structure of output
  str(vad,max.level = 1) 
```

```
## List of 4
##  $ p.values : num [1:6, 1:6] NA 0.4023 0.3439 0.0513 0.5306 ...
##   ..- attr(*, "dimnames")=List of 2
##  $ estimates: num [1:6, 1:6] NA NA 0.0956 NA NA ...
##   ..- attr(*, "dimnames")=List of 2
##  $ stats    :List of 5
##  $ plots    :List of 36
##   ..- attr(*, "dim")= int [1:2] 6 6
##   ..- attr(*, "dimnames")=List of 2
```

```r
  # don't worry about stats length, it is linked list
  str(vad$stats,1)
```

```
## List of 5
##  $ Vn1:List of 5
##  $ Vf1:List of 4
##  $ Vn2:List of 3
##  $ Vf2:List of 2
##  $ Vs1:List of 1
```

```r
  # p-values
  vad$p.values
```

```
##            Vn1       Vf1       Vn2        Vf2       Vs1       Vs2
## Vn1         NA 0.4022712 0.3438686 0.05133583 0.5306429 0.9373770
## Vf1 0.40227115        NA 0.4831033 0.46968388 0.1245342 0.9222038
## Vn2 0.34386863 0.4831033        NA 0.72916994 0.1055804 0.2698548
## Vf2 0.05133583 0.4696839 0.7291699         NA 0.2853680 0.1162736
## Vs1 0.53064294 0.1245342 0.1055804 0.28536802        NA        NA
## Vs2 0.93737704 0.9222038 0.2698548 0.11627358        NA        NA
```

```r
  # estimates, some estimates undefined so NA
  vad$estimates
```

```
##            Vn1 Vf1        Vn2 Vf2       Vs1       Vs2
## Vn1         NA  NA 0.09564013  NA        NA        NA
## Vf1         NA  NA         NA  NA        NA        NA
## Vn2 0.09564013  NA         NA  NA        NA        NA
## Vf2         NA  NA         NA  NA        NA        NA
## Vs1         NA  NA         NA  NA        NA 0.1408816
## Vs2         NA  NA         NA  NA 0.1408816        NA
```

```r
  # stats, i.e.
  vad$stats$Vn1$Vn2
```

```
## $p.value
## [1] 0.3438686
## 
## $estimate
##        cor 
## 0.09564013 
## 
## $test
## [1] "pearson"
## 
## $stats
## 
## 	Pearson's product-moment correlation
## 
## data:  num1 and num2
## t = 0.95115, df = 98, p-value = 0.3439
## alternative hypothesis: true correlation is not equal to 0
## 95 percent confidence interval:
##  -0.1027074  0.2866730
## sample estimates:
##        cor 
## 0.09564013 
## 
## 
## $vNames
## [1] "Vn1" "Vn2"
```

Let's look at plots:


```r
  # plots
  cowplot::plot_grid(plotlist = as.vector(vad$plots),nrow = dim(vad$plots)[1])
```

![](README_files/figure-html/unnamed-chunk-5-1.png)<!-- -->

Both matrix indexing and name indexing possible for plots:

```r
  #matrix indexing
  vad$plots[["Vn2","Vs1"]]
```

<img src="README_files/figure-html/unnamed-chunk-6-1.png" width="200" height="200" />

```r
  #name indexing
  vad$plots$Vn2.Vs1
```

<img src="README_files/figure-html/unnamed-chunk-6-2.png" width="200" height="200" />

```r
  vad$plots$Vs1.Vs2
```

<img src="README_files/figure-html/unnamed-chunk-6-3.png" width="200" height="200" />

You may notice `Surv-Surv` plot is a blank ggplot because it is undefined. 

### Parallelizing




```r
#if you have only numeric variables, use serial version
#if you have mixed or factor variables, use parallel version 

# serial
tictoc::tic()
vad = varDesc::cpvd(dta = dta)
tictoc::toc()

# parallel
no_cores <- parallel::detectCores() - 1
cl <- parallel::makeCluster(no_cores)
tictoc::tic()
vad = varDesc::cpvd(dta = dta,cl4par = cl)
tictoc::toc()
parallel::stopCluster(cl)
```
For parallel version, user should create a cluster with `parallel::makeCluster(...)` beforehand to give it to the function. I thought this provides huge flexibility in parallel version. One may want to use some function from a package for his/her own implementation of a test.In this case, all we need is to just export this package to the cluster that we initialized. 

### Customization

#### Changing the function

Let's say, you want to calculate Harrell's c-index (concordance index) instead of log-rank test(default) for `numeric-Surv`. You can change the pipeline by writing your function and configuring look-up table accordingly.


```r
#My function for numeric-Surv
myFunction<-function(num, sur, var.names){
  conc<-survcomp::concordance.index(num, sur[,1], sur[,2])
  re<-list()
  re$p.value <- conc$p.value
  re$estimate <- conc$c.index
  re$test <- "concordance"
  re$stats <- conc

  return(re)
}

#!newly defined function should be in working environment
newLookup<-
varDesc::customise.lookup(var_type = "numeric",
                          compare.set =c("Surv"), 
                          myfunctions = c("myFunction"))
newLookup
```

```
##         factor  numeric      Surv        
## factor  "fa.fa" "fa.nu"      "fa.Su"     
## numeric "fa.nu" "nu.nu"      "myFunction"
## Surv    "fa.Su" "myFunction" "Su.Su"
```

```r
vad = varDesc::cpvd(dta = dta,lookup = newLookup)

# a numeric-Surv pair
vad$stats$Vn1$Vs1
```

```
## $p.value
## [1] NA
## 
## $estimate
## [1] 0.5149802
## 
## $test
## [1] "concordance"
## 
## $stats
## $stats$c.index
## [1] 0.5149802
## 
## $stats$se
## [1] NA
## 
## $stats$lower
## [1] 0.3020843
## 
## $stats$upper
## [1] 0.7225748
## 
## $stats$p.value
## [1] NA
## 
## $stats$n
## [1] 100
## 
## $stats$data
## $stats$data$x
##   [1] 0.9148060435 0.9370754133 0.2861395348 0.8304476261 0.6417455189
##   [6] 0.5190959491 0.7365883146 0.1346665972 0.6569922904 0.7050647840
##  [11] 0.4577417762 0.7191122517 0.9346722472 0.2554288243 0.4622928225
##  [16] 0.9400145228 0.9782264284 0.1174873617 0.4749970816 0.5603327462
##  [21] 0.9040313873 0.1387101677 0.9888917289 0.9466682326 0.0824375581
##  [26] 0.5142117843 0.3902034671 0.9057381309 0.4469696281 0.8360042600
##  [31] 0.7375956178 0.8110551413 0.3881082828 0.6851697294 0.0039483388
##  [36] 0.8329160803 0.0073341469 0.2076589728 0.9066014078 0.6117786434
##  [41] 0.3795592405 0.4357715850 0.0374310329 0.9735399138 0.4317512489
##  [46] 0.9575765966 0.8877549055 0.6399787695 0.9709666104 0.6188382073
##  [51] 0.3334272113 0.3467482482 0.3984854114 0.7846927757 0.0389364911
##  [56] 0.7487953862 0.6772768302 0.1712643304 0.2610879638 0.5144129347
##  [61] 0.6756072745 0.9828171979 0.7595442676 0.5664884241 0.8496897186
##  [66] 0.1894739354 0.2712866147 0.8281584852 0.6932048204 0.2405447396
##  [71] 0.0429887960 0.1404790941 0.2163854151 0.4793985642 0.1974103423
##  [76] 0.7193558377 0.0078847387 0.3754899646 0.5144077083 0.0015705542
##  [81] 0.5816040025 0.1579052082 0.3590283059 0.6456318784 0.7758233626
##  [86] 0.5636468416 0.2337033986 0.0899805163 0.0856120649 0.3052183695
##  [91] 0.6674265147 0.0002388966 0.2085699569 0.9330341273 0.9256447486
##  [96] 0.7340943010 0.3330719834 0.5150633298 0.7439746463 0.6191592400
## 
## $stats$data$surv.time
##   [1] 1106  645 2172  141 1200 1481 2799  245  574  362  106 1343 1121 1308
##  [15]   65   96  222 1590  175 1559 1143  522  574 1132 3146  735  431  449
##  [29] 1871 2746 2038 1888   38 1696  932 1130 2226  834  617  552 1885 2259
##  [43] 1487 2231 1584   16  578 1126 1657 2688  139 1732 1111 2184 1203   68
##  [57]  313  400 1496 1071 1727 2274 1371  873 2223 1367 1792  616 3076 1450
##  [71]  911 1120  293  714  324 1808  701  193  182 1935  454  194  454 1471
##  [85]  563 3668 1759  307 2125 1043 1291 2963  433 2408  822 1416 1946   69
##  [99] 1520 2701
## 
## $stats$data$surv.event
##   [1] 0 1 0 0 1 0 0 1 0 0 1 1 1 0 1 0 1 1 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 1 1
##  [36] 0 0 1 0 1 0 0 0 0 1 0 1 0 0 0 1 0 1 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0
##  [71] 0 0 0 0 0 0 1 0 1 0 1 0 0 0 0 0 0 1 0 0 0 0 0 0 1 0 0 1 0 0
## 
## 
## $stats$comppairs
## [1] 3538
## 
## 
## $vNames
## [1] "Vn1" "Vs1"
```

Logic is same for plotting as well. Let's plot numeric variables versus survival times and color w.r.t. event in Surv. 


```r
#My function for plotting numeric-Surv
myPlotFunction<-function(num, sur, var.names){
  
  df<-data.frame(num,surv.times=sur[,1], event=as.factor(sur[,2]))
  colnames(df)[1]<-var.names[1] #names of numeric variable
  
  gg<-ggplot2::ggplot(df,ggplot2::aes_string(x=var.names[1],y="surv.times")) +
    ggplot2::geom_point(ggplot2::aes(color=event))
  
  return(gg)
}

#!newly defined function should be in working environment
newPlotLookup<-
  varDesc::customise.lookup(var_type = "numeric",
                            compare.set =c("Surv"), 
                            myfunctions = c("myPlotFunction"),vis = T)
newPlotLookup
```

```
##         factor     numeric          Surv            
## factor  "fa.fa_gg" "fa.nu_gg"       "fa.Su_gg"      
## numeric "nu.fa_gg" "nu.nu_gg"       "myPlotFunction"
## Surv    "Su.fa_gg" "myPlotFunction" "Su.Su_gg"
```

```r
vad = varDesc::cpvd(dta = dta,lookup.vis=newPlotLookup,vis = T)

# a numeric-Surv pair
vad$plots[["Vn1","Vs1"]]
```

<img src="README_files/figure-html/unnamed-chunk-10-1.png" width="300" height="300" />

#### Adding a new variable type

Hypothetically, `varDesc` is flexible to support different variable types. All we need is to define a new look-up filled with proper functions that we wrote for our purposes.  

## Other functions in the package 

```r
#returns default lookup tables
lups=varDesc::get_default_lookups()

#default lookup table for stat calculation 
lups$Lup4stats
```

```
##         factor  numeric Surv   
## factor  "fa.fa" "fa.nu" "fa.Su"
## numeric "fa.nu" "nu.nu" "nu.Su"
## Surv    "fa.Su" "nu.Su" "Su.Su"
```

```r
#default function for factor-factor
varDesc:::fa.fa
```

```
## function (fac1, fac2, var.names = NA) 
## {
##     valid = !(is.na(fac1) | is.na(fac2))
##     fac1 = fac1[valid]
##     fac2 = fac2[valid]
##     tab = table(fac1, fac2, dnn = var.names)
##     tried <- try(ft <- fisher.test(tab), silent = T)
##     if (inherits(tried, "try-error")) {
##         tried <- try(ft <- fisher.test(tab, simulate.p.value = T, 
##             B = 1e+07), silent = T)
##         if (inherits(tried, "try-error")) {
##             message("something wrong for fisher !...")
##             ft <- list(p.value = NA)
##         }
##     }
##     if (is.null(ft$estimate)) 
##         ft$estimate <- NA
##     re <- list()
##     re$p.value <- ft$p.value
##     re$estimate <- ft$estimate
##     re$test <- "fisher.test"
##     re$stats <- ft
##     return(re)
## }
## <environment: namespace:varDesc>
```

```r
#default lookup table for plotting 
lups$Lup4plots
```

```
##         factor     numeric    Surv      
## factor  "fa.fa_gg" "fa.nu_gg" "fa.Su_gg"
## numeric "nu.fa_gg" "nu.nu_gg" "nu.Su_gg"
## Surv    "Su.fa_gg" "Su.nu_gg" "Su.Su_gg"
```

```r
#default plot for factor-numeric
varDesc:::fa.nu_gg
```

```
## function (fac, num, var.names = NA) 
## {
##     dfbox <- data.frame(fac, num)
##     colnames(dfbox) <- var.names
##     gg <- ggplot2::ggplot(dfbox, ggplot2::aes_string(var.names[1], 
##         var.names[2])) + ggplot2::geom_boxplot()
##     return(gg)
## }
## <environment: namespace:varDesc>
```

```r
#helps to change lookup tables
newLookup<-
varDesc::customise.lookup(var_type = "new_type_X",
                          compare.set =c("numeric","factor","new_type_Y"), 
                          myfunctions = c("f_X.num","f_X.fac","f_X.Y"))


newLookup
```

```
##            factor    new_type_X new_type_Y numeric   Surv   
## factor     "fa.fa"   "f_X.fac"  NA         "fa.nu"   "fa.Su"
## new_type_X "f_X.fac" NA         "f_X.Y"    "f_X.num" NA     
## new_type_Y NA        "f_X.Y"    NA         NA        NA     
## numeric    "fa.nu"   "f_X.num"  NA         "nu.nu"   "nu.Su"
## Surv       "fa.Su"   NA         NA         "nu.Su"   "Su.Su"
```

```r
#prints the templates for user defined functions
varDesc::function_templates()
```

```
## !Be carefull: order of variables as inputs has to be same with the order of class names!
```

```
## 
## template of stat function: 
## function (aaa, bbb, var.names = NA) 
## {
##     re <- list()
##     re$p.value <- NA
##     re$estimate <- NA
##     re$test <- "UNDEFINED"
##     re$stats <- NA
##     return(re)
## }
## 
## template of plot function: 
## function (aaa, bbb, var.names = NA) 
## {
##     gg <- ggplot2::ggplot()
##     return(gg)
## }
```

